package com.zq.service.impl;

import com.zq.dao.CustomerDao;
import com.zq.factory.FactoryBean;
import com.zq.service.CustomerService;


public class CustomerServiceImpl implements CustomerService {
    private CustomerDao customerDao = (CustomerDao)FactoryBean.getFactoryBean().getBean("CUSTOMERDAO");
    @Override
    public void saveCustomer() {
        customerDao.saveCustomer();
    }
}
