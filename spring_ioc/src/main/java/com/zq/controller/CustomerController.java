package com.zq.controller;

import com.zq.factory.FactoryBean;
import com.zq.service.CustomerService;

public class CustomerController {
    private static  CustomerService customerService =(CustomerService) FactoryBean.getFactoryBean().getBean("CUSTOMERSERVICE");
    public static void main(String[] args) {
        customerService.saveCustomer();
    }
}
