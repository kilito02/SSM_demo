package com.zq.factory;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class FactoryBean {
    private static FactoryBean factoryBean;
    private static Properties properties;
    static{
        factoryBean = new FactoryBean();
        properties = new Properties();
        InputStream inputStream = FactoryBean.class.getClassLoader().getResourceAsStream("bean.properties");
        try {
            properties.load(inputStream);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    public static FactoryBean getFactoryBean(){
        return factoryBean;
    }
    public Object getBean(String className){
        Object obj=new Object();
        String property = properties.getProperty(className);
        try {
            obj=Class.forName(property).newInstance();
        }catch (Exception e) {
            e.printStackTrace();
        }
        return obj;
    }
}
